package seleniumgluecode;

import cucumber.api.java.Before;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 * @author Pablo Valenzuela Gomez
 * @version 1.0
 * Descripción de la clase: inicia y levanta una instancia del navegador Chrome y setea una URL para que se muestre
 * maximizada
 * */

public class Hooks {

    private static ChromeDriver driver;

    @Before
    public void setup(){

        System.setProperty("webdriver.chrome.driver","./src/test/resources/chromedriver/chromedriver.exe");
        driver = new ChromeDriver();
        driver.get("https://orangehrm-demo-6x.orangehrmlive.com/");
        driver.manage().window().maximize();



    }
   // @After
   // public void tearDown(){
   //     driver.quit();

    public static ChromeDriver getDriver(){
        return driver;
    }
}
