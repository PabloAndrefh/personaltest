package seleniumgluecode;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;


public class Test {

    private ChromeDriver driver = Hooks.getDriver();



    @Given("^El usuario se encuentra autenticado y en la página HOME de OrangeHRM$")
    public void el_usuario_se_encuentra_autenticado_y_en_la_página_HOME_de_OrangeHRM() throws Throwable {


        WebElement clickLogin = driver.findElement(By.xpath("//*[@id=\"btnLogin\"]"));
        clickLogin.click();

    String tituloPaginaHome = "OrangeHRM";
    Assert.assertEquals(tituloPaginaHome,driver.getTitle());

    }


    @When("^Ingrese al modulo Agregar Empleado y complete el formulario con la información básica solicitada y de click sobre el botón GUARDAR$")
    public void ingrese_al_modulo_Agregar_Empleado_y_complete_el_formulario_con_la_información_básica_solicitada_y_de_click_sobre_el_botón_GUARDAR() throws Throwable {

        Formulario.agregarEmpleado();

    }

    @Then("^El empleado es agregado exitosamente al aplicativo quedando disponible para ser consultado en módulo Lista de empleados$")
    public void el_empleado_es_agregado_exitosamente_al_aplicativo_quedando_disponible_para_ser_consultado_en_módulo_Lista_de_empleados() throws Throwable {

        ValidaEmpleado.ValidaEmpleado();

        String seccionPersonal = "Personal Details";
        Assert.assertEquals(seccionPersonal,driver.getTitle());

    }


}
