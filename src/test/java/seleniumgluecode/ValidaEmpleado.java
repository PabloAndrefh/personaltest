package seleniumgluecode;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 * @author Pablo Valenzuela Gomez
 * @version 1.0
 * Descripcion de la clase: realiza consulta a listado de empleados ingresando un Id especifico.
* */


public class ValidaEmpleado {
    public static ChromeDriver driver = Hooks.getDriver();

    /**
     * Este metodo ingresa al menú lateral izquierdo, selecciona PIM > List Employee y consulta por iD de Empleado
     */

    public static void ValidaEmpleado(){

        WebElement clickPim = driver.findElement(By.xpath("//div[4]/ul/li[2]/a/span[2]"));
        clickPim.click();
        WebElement listaEmpleado = driver.findElement(By.id("menu_pim_viewEmployeeList"));
        listaEmpleado.click();
        WebElement buscaEmpleado = driver.findElement(By.id("employee_name_quick_filter_employee_list_value"));
        buscaEmpleado.sendKeys("15970405");
        WebElement idEmpleado =driver.findElement(By.xpath("//div/div/div/table/tbody/tr/td[2]"));
        idEmpleado.click();

    }
}
