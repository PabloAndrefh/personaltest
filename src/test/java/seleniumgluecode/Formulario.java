package seleniumgluecode;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

/**
 * @author Befh
 * @version 1.0
 * Descripcion de la clase: realiza incorporación de empleado a plataforma OrangeHRM a través del módulo PIM
 * sistema OrangeHRM
 */
public class Formulario {

    public static ChromeDriver driver = Hooks.getDriver();

    /**
     * Metodo que hace realiza la interacción con el menú lateral izquierdo, ingresando a la opcion PIM > Add Employee,
     * completa el formulario que consta de 3 pasos y lo guarda en el sistema
     */
    public static void agregarEmpleado() {

        WebElement clickPim = driver.findElement(By.xpath("//div[4]/ul/li[2]/a/span[2]"));
        clickPim.click();
        WebElement clickAddEmploye = driver.findElement(By.xpath("//*[@id=\"menu_pim_addEmployee\"]/span[2]"));
        clickAddEmploye.click();

        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

        //Primer formulario
        WebElement primerNombre = driver.findElement(By.id("firstName"));
        primerNombre.sendKeys("Pablo");
        WebElement segundoNombre = driver.findElement(By.id("middleName"));
        segundoNombre.sendKeys("Andres");
        WebElement apellido = driver.findElement(By.id("lastName"));
        apellido.sendKeys("Valenzuela");
        WebElement idEmpleado = driver.findElement(By.id("employeeId"));
        idEmpleado.clear();
        idEmpleado.sendKeys("15970405");
        WebElement locacion = driver.findElement(By.xpath("//div/sf-decorator[2]/div/div/input"));
        locacion.click();
        driver.findElement(By.xpath("//span[contains(text(),  \"Australian Regional HQ\")]")).click();

        WebElement siguiente = driver.findElement(By.id("systemUserSaveBtn"));
        siguiente.click();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

        //acá comienza el segundo formulario

        WebElement otroId = By.id("otherId").findElement(driver);
        otroId.sendKeys("15970405");
        WebElement fechaNac = driver.findElement(By.id("emp_birthday"));
        fechaNac.sendKeys("Tue, 25 Dec 1984");
        WebElement numLicencia = driver.findElement(By.id("licenseNo"));
        numLicencia.sendKeys("12345678");
        WebElement expLicencia = driver.findElement(By.id("emp_dri_lice_exp_date"));
        expLicencia.sendKeys("Fri, 25 Dec 2020");
        WebElement apodo = driver.findElement(By.id("nickName"));
        apodo.sendKeys("Prospecto");
        WebElement grupoSangre = driver.findElement(By.xpath("//*[@id=\"1_inputfileddiv\"]/div/input"));
        grupoSangre.click();
        driver.findElement(By.xpath("//span[contains(text(),  \"AB\")]")).click();
        WebElement pasatiempo = driver.findElement(By.id("5"));
        pasatiempo.sendKeys("Emprendimiento");
        WebElement sigintDos = driver.findElement(By.xpath("//ui-view/div[3]/button[2]"));
        sigintDos.click();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

        //acá comienza el tercer formulario

        WebElement fechaIngreso = driver.findElement(By.id("joined_date"));
        fechaIngreso.sendKeys("Wed, 15 Jul 2020");
        WebElement fechaTermino = driver.findElement(By.id("date_of_permanency"));
        fechaTermino.sendKeys("Wed, 31 Dec 2025");
        WebElement profesion = driver.findElement(By.xpath("//materializecss-decorator[2]/div/sf-decorator[1]/div/div/input"));
        profesion.click();
        driver.findElement(By.xpath("//span[contains(text(),  \"QA\")]")).click();
        WebElement tipoContrato = driver.findElement(By.xpath("//*[@id=\"employment_status_id_inputfileddiv\"]/div/input"));
        tipoContrato.click();
        driver.findElement(By.xpath("//span[contains(text(),  \"Freelance\")]")).click();
        WebElement subarea = driver.findElement(By.xpath("//*[@id=\"subunit_id_inputfileddiv\"]/div/input"));
        subarea.click();
        driver.findElement(By.xpath("//span[contains(text(),  \"QA Team\")]")).click();
        WebElement fechaContrato = driver.findElement(By.xpath("//materializecss-decorator[7]/div/sf-decorator[2]/div/input"));
        fechaContrato.sendKeys("Mon, 20 Jul 2020");
        WebElement region = driver.findElement(By.xpath("//materializecss-decorator[1]/div/sf-decorator[1]/div/div/input"));
        region.click();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.findElement(By.xpath("//span[contains(text(),  \"Region-2\")]")).click();

        WebElement fte = driver.findElement(By.xpath("//materializecss-decorator[1]/div/sf-decorator[2]/div/div/input"));
        fte.click();
        driver.findElement(By.xpath("//span[contains(text(),  \"0.5\")]")).click();
        WebElement deptoTemporal = driver.findElement(By.xpath("//materializecss-decorator[1]/div/sf-decorator[3]/div/div/input"));
        deptoTemporal.click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.xpath("//span[contains(text(),  \"Sub unit-1\")]")).click();

        WebElement sigintTres = driver.findElement(By.xpath("//div[2]/ui-view/div[3]/button[3]"));
        sigintTres.click();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }
}
