Feature: Agregar Empleado
  Como usuario administrador quiero agregar un empleado en OrangeHRM para registrarlo en la plataforma

  Scenario: Agregar un empleado desde el módulo de información personal MID
    Given El usuario se encuentra autenticado y en la página HOME de OrangeHRM
    When Ingrese al modulo Agregar Empleado y complete el formulario con la información básica solicitada y de click sobre el botón GUARDAR
    Then El empleado es agregado exitosamente al aplicativo quedando disponible para ser consultado en módulo Lista de empleados


